# Individual Mining Components #



### 0. Pre-requirements ###

Have an Android application with a event log in format .XES storage in "assets" folder and named "event_log.xes"

### 1. Add dependencies in gradle ###

    implementation 'com.google.code.gson:gson:2.8.2'

    //for retrofit
    implementation 'com.squareup.retrofit2:retrofit:2.4.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.1.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.4.1'


    implementation "io.swagger:swagger-annotations:$swagger_annotations_version"
    implementation "com.google.code.gson:gson:2.8.5"
    testImplementation"junit:junit:$junit_version"

    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'androidx.appcompat:appcompat:1.0.0'
    implementation 'androidx.constraintlayout:constraintlayout:1.1.3'
    androidTestImplementation 'androidx.test.ext:junit:1.1.1'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.1.0'
    implementation 'com.google.android.gms:play-services-ads:15.0.1'


    implementation 'androidx.room:room-runtime:2.0.0'
    annotationProcessor 'androidx.room:room-compiler:2.0.0'

    implementation 'com.android.volley:volley:1.1.0'

    //MQTT
    implementation 'org.eclipse.paho:org.eclipse.paho.client.mqttv3:1.1.1'
    implementation 'org.eclipse.paho:org.eclipse.paho.android.service:1.1.1'
    implementation 'androidx.work:work-runtime:2.5.0'


    // FOR GENERATE SCHEDULED TASK
    def work_version = "2.5.0"
    // (Java only)
    implementation "androidx.work:work-runtime:$work_version"
    // optional - RxJava2 support
    implementation "androidx.work:work-rxjava2:$work_version"
    // optional - GCMNetworkManager support
    implementation "androidx.work:work-gcm:$work_version"
    // optional - Test helpers
    androidTestImplementation "androidx.work:work-testing:$work_version"
    // optional - Multiprocess support
    implementation "androidx.work:work-multiprocess:$work_version"


### 2. Add the following to your manifest ###

    <uses-permission android:name="android.permission.INTERNET"/>
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE"/>
    <uses-permission android:name="android.permission.WAKE_LOCK"/>
    <application ...>
        <activity ...>
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
        <service android:name="org.eclipse.paho.android.service.MqttService"/>
        <service android:name=".fpm.individualpmmodule.patternqueryresolver.org.openapitools.server.service.MQTTService" android:exported="false" android:enabled="true" android:permission="android.permission.BIND_JOB_SERVICE"> </service>
    </application>
    <uses-permission android:name="android.permission.INTERNET"/>

### 3. Add library in the application ###

Add all the *fpm* sources in your application.

### 4. Update params ###

- In file patternqueryresolver.networking.ApiManager you must change the IP of the Social Mining Component server
- In file patternqueryresolver.org.openapitools.server.service.MQTTConfiguration you must change the IP of the MQTT broker (the same that the previous one).


### 5. Start services in Main Activity ###

Add the following code in your main activity to run services that receives queries and generate user model:

    private Intent mServiceIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

	//...

        startServiceMQTT();
        startGenerateModelService();
    }


    private void startServiceMQTT() {
        MQTTService service = new MQTTService();
        mServiceIntent = new Intent(this, service.getClass());


        boolean run = isMyServiceRunning(service.getClass());
        Log.d(TAG, " - Run1: " + run);
        if (!isMyServiceRunning(service.getClass())) {
            startService(mServiceIntent);

        }
        Log.d(TAG, " - Run1: " + run);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service status", "Running");
                return true;
            }
        }
        Log.i("Service status", "Not running");
        return false;
    }

    private boolean startGenerateModelService(){

        //Constraints constraints = new Constraints.Builder().setRequiresBatteryNotLow(true).build();

        Toast.makeText(getApplicationContext(), "Starting ", Toast.LENGTH_LONG).show();

        WorkRequest saveRequest =
                new PeriodicWorkRequest.Builder(GenerateModelService.class,
                        1, TimeUnit.DAYS,
                        2, TimeUnit.HOURS)
                        .build();
        WorkManager
                .getInstance(getApplicationContext())
                .enqueue(saveRequest);

        return true;
    }