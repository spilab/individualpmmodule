package es.unex.spilab.alphaprocessmining.fpm.individualpmmodule;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.model.Pattern;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.patternqueryresolver.CheckPatternAsyncTask;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.patternqueryresolver.SendTracesAsyncTask;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.ProcessMiningAsyncTask;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.alphaalgorithm.WorkflowNetwork;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.tools.LocalPersistence;

public class FPMApi {
    /*event log name*/
    private static final String event_log_name = Parameters.EVENT_LOG_NAME;
    /*for log*/
    private static final String TAG = "FPMApi";
    /*Android parameters for FPM API*/
    private Context context;


    public interface Callback{
        void callbackCall(boolean status);
    }

    /*Internal variables*/
    //private WorkflowNetwork mModel=null;


    public FPMApi(Context context){
        this.context = context;
    }


    public void generateModel(Callback callback){
        try{

            String eventLog = this.event_log_name;
            ProcessMiningAsyncTask generateTask = new ProcessMiningAsyncTask(this.context, network -> {
                if (network == null){
//                    Toast.makeText(this.context, "Error generating model", Toast.LENGTH_LONG).show();
                    Log.i(TAG, "Error generating model");
                    callback.callbackCall(false);
                }
                else{
                    LocalPersistence.writeObjectToFile(this.context, network, "model");
                    //this.mModel = network;
                    callback.callbackCall(true);
                }
            });
            if (Build.VERSION.SDK_INT >= 11) {
                generateTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, eventLog);
            } else {
                generateTask.execute(eventLog);
            }
            //generateTask.execute(eventLog);
            //WorkflowNetwork model = this.generateModel("edited_hh102_labour.xes");

        } catch (Exception e){
            //Toast.makeText(this.context, "Error generating model", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Error generating model");
        }
    }


    public WorkflowNetwork getModel(){
        WorkflowNetwork network = (WorkflowNetwork) LocalPersistence.readObjectFromFile(this.context, "model");
        if(network==null){
//            Toast.makeText(this.context, "Null model", Toast.LENGTH_LONG).show();
            Log.i(TAG, "Null model");
        }
        return network;
    }

    public void processQuery(final String patternString){
        WorkflowNetwork model = getModel();
        if (model==null) {
            generateModel(new Callback() {
                @Override
                public void callbackCall(boolean status) {
                    if (status==true){
                        WorkflowNetwork model = getModel();
                        processQueryAux(patternString, model);
                    }
                }
            });

        } else {
            processQueryAux(patternString, model);
        }
    }

    private void processQueryAux(String patternString, WorkflowNetwork mModel){
        Pattern pattern = new Pattern(patternString);

        if(mModel!=null){
            new CheckPatternAsyncTask(this.context, mModel, containsPattern -> {
                if (containsPattern){
                    Log.i("PATRON", "Let's check the traces. Calling async task...");
//                    Toast.makeText(this.context, "Your behaviour match!!", Toast.LENGTH_LONG).show();
                    Log.i(TAG, "Your behaviour match!!");
                    new SendTracesAsyncTask(this.context, this.event_log_name, pattern, () -> {
                        //Finished
                    }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                else{
                    //Toast.makeText(this.context, "Your behaviour doesn't match", Toast.LENGTH_LONG).show();
                    Log.i(TAG, "Your behaviour doesn't match");
                }

            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pattern);
        }
            else{
//            Toast.makeText(this.context, "Cancelled! Model is null", Toast.LENGTH_SHORT).show();
            Log.i(TAG, "Cancelled! Model is null");
        }
    }

}
