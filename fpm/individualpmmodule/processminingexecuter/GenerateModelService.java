package es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Random;

import es.unex.spilab.alphaprocessmining.MainActivity;
import es.unex.spilab.alphaprocessmining.R;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.FPMApi;



public class GenerateModelService extends Worker{

    private FPMApi fpmApi = null;
    private Context context;
    private static String  CHANNEL_ID = "300078";
    public GenerateModelService(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
        fpmApi = new FPMApi(context);
        //this.createNotificationChannel();
    }


    private void showNotification(String title, String body) {

        //Intent to open APP when click in the notification.
        Intent resultIntent = new Intent(context, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID="1";

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel= new NotificationChannel(CHANNEL_ID, "FPMGenModel", NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription ("Channel to publish that a model of FPM is generated");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder= new NotificationCompat.Builder(context, CHANNEL_ID);
        notificationBuilder.setAutoCancel(true).setAutoCancel(true).setDefaults(Notification.DEFAULT_ALL).setContentIntent(resultPendingIntent).setWhen(System.currentTimeMillis()).setSmallIcon(android.R.drawable.alert_light_frame).setContentTitle(title).setContentText(body);
        notificationManager.notify((new Random().nextInt()),notificationBuilder.build());

    }



    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "FPMGenModel";
            String description = "Channel to publish that a model of FPM is generated";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(this.CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public Result doWork() {

        Toast.makeText(context,"GenModel running", Toast.LENGTH_SHORT).show();
        if (fpmApi!=null){
            fpmApi.generateModel(status -> {
                if(status==true){
                    Toast.makeText(context,"GenModel true", Toast.LENGTH_SHORT).show();
                    showNotification("Generate Model Service", "Model generated succesfully");
                    /*NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_launcher_foreground)
                            .setContentTitle(R.string.app_name+"- Generate Model Service")
                            .setContentText("Model generated succesfully")
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);*/
                }
                else{
                    Toast.makeText(context,"GenModel false", Toast.LENGTH_SHORT).show();
                    showNotification("Generate Model Service", "Problems generating model in FPMApi");
                    /*NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_launcher_foreground)
                            .setContentTitle(R.string.app_name+"- Generate Model Service")
                            .setContentText("Problems generating model in FPMApi")
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);*/
                }
            });
            return Result.success();
        } else{
            Toast.makeText(context,"GenModel failure", Toast.LENGTH_SHORT).show();
            showNotification("Generate Model Service", "Problems with task");
            /*NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle(R.string.app_name+"- Generate Model Service")
                    .setContentText("Problems with task")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);*/
            return Result.failure();
        }
    }

}
