package es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import es.unex.spilab.alphaprocessmining.MainActivity;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.alphaalgorithm.AlphaAlgorithm;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.alphaalgorithm.Utils;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.alphaalgorithm.WorkflowNetwork;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.alphaalgorithm.tools.Event;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.alphaalgorithm.tools.Place;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.alphaalgorithm.tools.Trace;

public class ProcessMiningAsyncTask extends AsyncTask<String, Void,WorkflowNetwork> {


    private final Context mContext;
    private String eventLog;
    private TaskDelegate delegate;

    public interface TaskDelegate {
        public void taskCompletionResult(WorkflowNetwork network);
    }


    public ProcessMiningAsyncTask(Context context, TaskDelegate delegate){
        this.mContext = context;
        this.eventLog = null;
        this.delegate = delegate;
    }

    @Override
    protected WorkflowNetwork doInBackground(String... eventLog) {
        this.eventLog=eventLog[0];
        Log.i("HOLA", "generando");
        return generateModel(this.mContext, this.eventLog);
    }

    private WorkflowNetwork generateModel(Context context, String eventLogName){
        //Validate.validateAlgorithmPercentage();
        //Set<Trace> eventLog = checkArgs(new String[]{"d","L1"});
        Set<Trace> eventLog = new HashSet<>();
        try {
            eventLog = Utils.parseXESFile(context,eventLogName);
            Log.i(MainActivity.TAG, eventLog.toString());
            Log.i(MainActivity.TAG, String.valueOf(eventLog.size()));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        AlphaAlgorithm.takeInAccountLoopsLengthTwo = true;
        WorkflowNetwork model = AlphaAlgorithm.discoverWorkflowNetwork(eventLog);

        Log.i("IN",model.getIn().toString());
        Log.i("OUT",model.getOut().toString());

        Log.i("EVENTS",model.getEventsList().toString());
        Log.i("TRANSITIONS", model.getEventToPlaceTransitionsMap().toString());
        Log.i("TRANSITIONS", model.getEventToPlaceTransitions().toString());
        Log.i("TRANSITIONS", model.getPlaceToEventTransitions().toString());
        Map<Event, Set<Place>> transitions = model.getEventToPlaceTransitionsMap();
        System.out.println(model.getWorkflowPlaces().toString());
        for(Place place: model.getWorkflowPlaces()){
            System.out.println("Place: "+place.toString());
        }
        for( Event event: transitions.keySet()){
            System.out.println("Event: "+event.toString()+"="+transitions.get(event));
        }

        return model;
    }

    @Override
    protected void onPostExecute(WorkflowNetwork workflowNetwork) {
        super.onPostExecute(workflowNetwork);
        this.delegate.taskCompletionResult(workflowNetwork);


    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        this.delegate.taskCompletionResult(null);

    }
}
