package es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class Pattern {

    public static final String AND = "+";
    public static final String OR = ".";
    public static final String NEXT = "-";
    public static final String EXIST_NEXT = ",";
    public static final String NEGATION = "!";
    private final String stringPattern;
    List<String> events;
    List<Boolean> negations;
    List<String> operations;

    public Pattern (String pattern){
        this.stringPattern = pattern;
        this.patternToList(pattern);
    }

    private void patternToList(String pattern) {
        List<String> events = new ArrayList<>();
        List<String> operations = new ArrayList<>();
        List<Boolean> negations = new ArrayList<>();

        String[] split = pattern.split(Pattern.NEXT);
        Log.i("PATTERN", pattern);
        Log.i("PATTERNSPLIT", String.valueOf(split.length));
        for (int i = 0 ; i< split.length; i++){
            String [] split2 = split[i].split(Pattern.EXIST_NEXT);
            Log.i("PATTERNSPLIT2", String.valueOf(split2.length));
            if(split2.length==1){
                if(split[i].contains(Pattern.NEGATION)==true){
                    split[i] = split[i].replace(Pattern.NEGATION,"");
                    negations.add(true);
                }
                else{
                    negations.add(false);
                }
                events.add(split[i]);
            }
            else{
                for(int j=0; j<split2.length; j++){
                    if(split2[j].contains(Pattern.NEGATION)==true){
                        split2[j] = split2[j].replace(Pattern.NEGATION,"");
                        negations.add(true);
                    }
                    else{
                        negations.add(false);
                    }
                    events.add(split2[j]);
                    if(j<split2.length-1){
                        operations.add(Pattern.EXIST_NEXT);
                    }
                }
            }

            if(i<split.length-1){
                operations.add(Pattern.NEXT);
            }
        }

        this.events= events;
        Log.i("PATTERNLENGTH", String.valueOf(this.events.size()));
        this.operations = operations;
        this.negations = negations;

    }

    public String get(int index){
        return this.events.get(index).trim();
    }

    public Boolean isNegation(int index) { return this.negations.get(index);}

    public String getNextOperation(int index){
        if (index>=this.operations.size()){
            return null;
        }
        else{
            return this.operations.get(index);
        }
    }

    public int size(){
        return this.events.size();
    }

    public String getString(){
        return this.stringPattern;
    }
}
