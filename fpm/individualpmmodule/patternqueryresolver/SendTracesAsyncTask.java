package es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.patternqueryresolver;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.model.Pattern;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.model.Traces;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.alphaalgorithm.Utils;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.alphaalgorithm.tools.Event;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.processminingexecuter.alphaalgorithm.tools.Trace;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.patternqueryresolver.networking.ApiManager;
import es.unex.spilab.alphaprocessmining.fpm.individualpmmodule.patternqueryresolver.networking.ApiService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendTracesAsyncTask extends AsyncTask<Void, Void, Void> {

    private final Context mContext;
    private final String filename;
    private final Pattern pattern;
    private TaskDelegate delegate;

    public interface TaskDelegate {
        public void onFinished();
    }


    public SendTracesAsyncTask(Context context, String filename, Pattern pattern, TaskDelegate delegate){
        this.mContext = context;
        this.filename = filename;
        this.pattern = pattern;
        this.delegate = delegate;
    }


    @Override
    protected Void doInBackground(Void... voids) {
        Set<Trace> eventLog = new HashSet<>();
        Log.i("PATTERN", "inBackground1");
        try {
            eventLog = Utils.parseXESFile(this.mContext,this.filename);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.i("PATTERN", "inBackground2");

        String userId = getAdvertisingID();

        Traces traces = new Traces(userId, this.pattern.getString());
        for (Trace trace : eventLog){
            if(this.containsPattern(trace,pattern)){
                traces.addTrace(this.TraceToListString(trace));
            }
        }

        if(traces.numberOfTraces()>0){
            ApiService getResponse = ApiManager.getAPIService();
            Call<ResponseBody> call = getResponse.uploadTraces(traces);
            call.enqueue(new Callback() {
                @Override
                public void onResponse(Call call, Response response) {
                    if(response.isSuccessful()){
                        //TODO delete these toast
                        Toast.makeText(mContext, "Data sent to the server", Toast.LENGTH_LONG).show();
                    }
                    else{
                        Toast.makeText(mContext, "Data DOESN'T sent to the server", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call call, Throwable t) {
                    Toast.makeText(mContext, "Connection with server fail", Toast.LENGTH_LONG).show();
                }
            });
        }
        else{
            Log.i("NOCONTIENEPATRON", "Ninguna traza con el patrón");
        }

        return null;
    }

    private List<String> TraceToListString(Trace trace) {
        List<String> strings = new ArrayList<>();
        List<Event> eventList = trace.getEventsList(); /*trace events*/
        for (Event e : eventList){
            strings.add(e.getName());
        }
        return strings;
    }

    private boolean containsPattern(Trace trace, Pattern pattern) {
        int index = 0; /*index to indicate which pattern event we are looking for*/

        Log.i("PATTERN", "Buscando trazas con patrón");

        List<Event> eventList = trace.getEventsList(); /*trace events*/
        for (int i = 0; i < eventList.size(); i++){ /*index to move along the trace to be treated*/
            Event event = eventList.get(i);
            String eventpattern = pattern.get(index);
            boolean isNegative = pattern.isNegation(index);
            String operation = index > 0 ? pattern.get(index-1) : null;
            boolean encontrado = false;

            if (!isNegative){ //if we're looking to make it an event
                if(event.getName().equals(eventpattern)){ //if we find the event we were looking for
                    encontrado=true;/*we indicate that we have found the pattern chain*/
                    index++; /*we indicate that we move on to the next element of the pattern*/
                }
            }
            else{ //if we're looking to make it different from an event
                //if we're dealing with an operation Pattern.NEXT, we check in this position
                if(operation == Pattern.NEXT) {
                    if (!event.getName().equals(eventpattern)) { //if we find an event other than the one we were looking for
                        encontrado = true;/*we indicate that we have found the pattern chain*/
                        index++; /*we indicate that we move on to the next element of the pattern*/
                    }
                }
                else{ //if not, we look for that it doesn't exist from the index we're trying to get ahead
                    boolean existe=false;
                    for(int index2 = i; index2<eventList.size(); index2++){
                        if(eventList.get(index2).equals(eventpattern)){
                            existe=true;
                        }
                    }
                    if(!existe){
                        encontrado = true;/*we indicate that we have found the pattern chain*/
                        index++; /*we indicate that we move on to the next element of the pattern*/
                    }
                }
            }

            if(!encontrado){ //if we haven't found the pattern element
                if (operation==null || operation==Pattern.NEXT) {//if we were looking for a part of the pattern where the two events must go together...
                    index = 0;  //if we had already found previous elements of the pattern we discard them, we go back to look for the pattern from 0
                    //we indicate that we have not yet found elements of the pattern
                }
                else{
                    if(operation==Pattern.EXIST_NEXT){ //if what we're looking for is that the next event exists, even if elements get in the way...
                        //we wait to see if we find the pattern later
                    }
                }
            }
            if(index == pattern.size()){ /*if we've already found all the elements of the pattern, we stop looking*/
                break;
            }
        }

        if(index == pattern.size()){ //if we've found all the elements of the pattern, we can say that it contains
            Log.i("CONTIENEPATRON", pattern.getString());
            Log.i("CONTIENEPATRON", trace.toString());
            return true;
        }
        else{
            Log.i("NOCONTIENEPATRON", pattern.getString());
            Log.i("NOCONTIENEPATRON", trace.toString());
            return false;
        }
    }


    private String getAdvertisingID(){
        AdvertisingIdClient.Info info = null;
        try {
            info = AdvertisingIdClient.getAdvertisingIdInfo(this.mContext);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }
        return info.getId();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        this.delegate.onFinished();
    }
}